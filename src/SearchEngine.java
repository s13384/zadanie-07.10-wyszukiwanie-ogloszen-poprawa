import java.util.ArrayList;


public class SearchEngine {

	private ArrayList<IFilter> filters;
	private SearchSettings settings;
	
	public ArrayList<IFilter> getFilters() {
		return filters;
	}
	public void setFilters(ArrayList<IFilter> filters) {
		this.filters = filters;
	}
	public SearchSettings getSettings() {
		return settings;
	}
	public void setSettings(SearchSettings settings) {
		this.settings = settings;
	}
	
	public void addFilter(IFilter filter)
	{
		filters.add(filter);
	}
	public void removeFilter(IFilter filter)
	{
		filters.remove(filter);
	}
	public void performSearch(ArrayList<Advertisment> ads)
	{
		// sprawdzenie setingsow
		
		for(IFilter filter : filters)
		{
			if(filter.canFilter())
			{
				filter.doFilter(ads);
			}
		}
	}
}
