import java.util.ArrayList;


public class ByBrandFilter implements IFilter {

	private SearchSettings settings;
	
	@Override
	public void setSettings(SearchSettings settings) {
		
		this.settings=settings;

	}

	@Override
	public boolean canFilter() {
	
		if(settings.getBrand()!=null && !settings.getBrand().isEmpty()) return true;
		
		return false;
	}

	@Override
	public void doFilter(ArrayList<Advertisment> ads) {
		
		int i;
		
		for (i=0;i<=ads.size();i++)
		{
			if (ads.get(i).getCarInfo().getBrand()!=settings.getBrand())
			{
				ads.remove(i);
			}
		}
	}
}
