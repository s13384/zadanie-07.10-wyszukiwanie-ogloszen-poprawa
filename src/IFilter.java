import java.util.ArrayList;


public interface IFilter {

	public void setSettings(SearchSettings settings);
	public boolean canFilter();
	public void doFilter(ArrayList<Advertisment> ads);
}
