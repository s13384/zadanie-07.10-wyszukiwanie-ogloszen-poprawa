import java.util.ArrayList;
import java.util.Comparator;


public interface Sorter extends Comparator<Advertisment> {
	
	public ArrayList<Advertisment> sort(ArrayList<Advertisment> ads);
}
