import java.util.ArrayList;

public class SortEngine {

	private static ArrayList<Sorter> sorters;
	
	SortEngine()
	{
		// info gdzie jest jaki sortownik
		sorters.add(0, new SortByPrice());
		sorters.add(1, new SortByYear());
		sorters.add(2, new SortByDate());
		sorters.add(3, new SortByMilage());
	}
	public ArrayList<Advertisment> sortList(ArrayList<Advertisment> ads, int sortType)
	{
		if (sortType>=0 && sortType < sorters.size())
		{	
			ArrayList<Advertisment> tmpAds = ads;
			return sorters.get(sortType).sort(tmpAds);
		}
		return ads;
		
	}
}
