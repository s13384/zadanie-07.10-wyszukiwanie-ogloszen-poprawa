import java.util.ArrayList;
import java.util.Collections;


public class SortByDate implements Sorter {

	@Override
	public int compare(Advertisment ad1, Advertisment ad2) {
		
		if (ad1.getCarInfo().getDate().after(ad2.getCarInfo().getDate())) return 1;
		if (ad1.getCarInfo().getDate().before(ad2.getCarInfo().getDate())) return -1;
		
		return 0;
	}

	@Override
	public ArrayList<Advertisment> sort(ArrayList<Advertisment> ads) {

		Collections.sort(ads, this);
		return ads;
	}

}
